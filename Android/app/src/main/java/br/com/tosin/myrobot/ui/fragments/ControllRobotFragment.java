package br.com.tosin.myrobot.ui.fragments;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import br.com.tosin.myrobot.R;
import br.com.tosin.myrobot.ui.activities.MainActivity;
import br.com.tosin.myrobot.utils.RepeatListener;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ControllRobotFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ControllRobotFragment extends Fragment implements SensorEventListener {

    private ProgressBar progressBarLeft;
    private ProgressBar progressBarRight;

    private Button buttonBreak;
    private Button buttonGas;

    private SensorManager mSensorManager;
    private Sensor mSensor;

    /**
     * Sao 4 posicoes, são sempre positivo, sendo 0 sem uso
     *
     * 1 - para frente
     * 2 - para tras
     * 3 - esquerda
     * 4 - direita
     */
    private static String[] myData = new String[4];


    public ControllRobotFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ControllRobotFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ControllRobotFragment newInstance() {
        ControllRobotFragment fragment = new ControllRobotFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_controll_robot, container, false);

        confView(view);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSensorManager != null)
            mSensorManager.unregisterListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSensorManager != null && mSensor != null)
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void confView(View view) {
        mSensorManager = (SensorManager) view.getContext().getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

        progressBarLeft = (ProgressBar) view.findViewById(R.id.progressBarLeft);
        progressBarRight = (ProgressBar) view.findViewById(R.id.progressBarRight);

        buttonBreak = (Button) view.findViewById(R.id.buttonBreak);
        buttonGas = (Button) view.findViewById(R.id.buttonAccelerate);


        buttonBreak.setOnTouchListener(new RepeatListener(400, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myData[0] = String.valueOf(0);
                myData[1] = String.valueOf(1);
                MainActivity.getManagerBluetooth().sendMessage(myData, getActivity());
            }
        }));

        buttonGas.setOnTouchListener(new RepeatListener(400, 100, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myData[0] = String.valueOf(1);
                myData[1] = String.valueOf(0);
                MainActivity.getManagerBluetooth().sendMessage(myData, getActivity());
            }
        }));

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (mSensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            float side = event.values[0];
            int turn = Math.round(event.values[1]);

            if (side < 0)
                turn = turn * -1;

            // vira para direita
            if (turn > 0) {
                myData[2] = String.valueOf(0);
                myData[3] = String.valueOf(turn);

                progressBarLeft.setProgress(0);
                progressBarRight.setProgress(turn * 10);
            } else {
                myData[2] = String.valueOf(turn * -1);
                myData[3] = String.valueOf(0);

                progressBarLeft.setProgress(turn * -1 * 10);
                progressBarRight.setProgress(0);
            }
            MainActivity.getManagerBluetooth().sendMessage(myData, getActivity());
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
