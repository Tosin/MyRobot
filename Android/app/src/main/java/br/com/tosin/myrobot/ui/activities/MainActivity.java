package br.com.tosin.myrobot.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import app.akexorcist.bluetotohspp.library.BluetoothState;
import br.com.tosin.myrobot.R;
import br.com.tosin.myrobot.ui.fragments.ControllRobotFragment;
import br.com.tosin.myrobot.ui.fragments.NoConnectionFragment;
import br.com.tosin.myrobot.utils.Immersive;
import br.com.tosin.myrobot.utils.ManagerBluetooth;

public class MainActivity extends AppCompatActivity {

    private Immersive immersive;

    private static ManagerBluetooth managerBluetooth;
    private static Activity activity;

    private static FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        activity = this;
        fm = getSupportFragmentManager();

        immersive = new Immersive(this, R.id.layoutMain, 2000);
        getManagerBluetooth().initialiseBluetooth(this);

        changeFragment();

    }

    @Override
    protected void onDestroy() {
        getManagerBluetooth().destroy();
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        getManagerBluetooth().start(this);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        immersive.onWindowFocusChanged(hasFocus);

    }

    public static void changeFragment() {
        if (getManagerBluetooth().isConnected())
            fm.beginTransaction().replace(R.id.baseContainer, ControllRobotFragment.newInstance()).commit();
        else
            fm.beginTransaction().replace(R.id.baseContainer, NoConnectionFragment.newInstance()).commit();
    }

    public static ManagerBluetooth getManagerBluetooth() {
        if (MainActivity.managerBluetooth == null) {
            managerBluetooth = new ManagerBluetooth(activity);
        }
        return managerBluetooth;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if (resultCode == Activity.RESULT_OK)
                MainActivity.getManagerBluetooth().getBluetoothManager(this).connect(data);
        } else if (requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                MainActivity.getManagerBluetooth().getBluetoothManager(this).setupService();
                MainActivity.getManagerBluetooth().getBluetoothManager(this).startService(BluetoothState.DEVICE_ANDROID);
            } else {
                Snackbar.make(getCurrentFocus(), "Bluetoot não esta ativo", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    public static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.obj != null) {
                String text = (String) msg.obj;

//                if (text.contains("ARRIVED")){
//                    new CustomDialog(CustomDialog.COMPLETE).show(activity.getFragmentManager(), "missiles");
//                }
//                else if (text.contains("INCOMPLETE")){
//                    new CustomDialog(CustomDialog.INCOMPLETE).show(activity.getFragmentManager(), "missiles");
//                }
//                else if (text.contains("CRASH")){
//                    new CustomDialog(CustomDialog.ROBOT_CRASH).show(activity.getFragmentManager(), "missiles");
//                }
//                else {
//                    new CustomDialog(CustomDialog.ERROR).show(activity.getFragmentManager(), "missiles");
//                }
//                setSending();
            } else {
//                if (msg.arg1 == 0) {
//                    setNotSending();
//                    floatingConnection.setImageResource(R.drawable.bluetooth_disconnect);
//                }
//                if (msg.arg1 == 1) {
//                    setSending();
//                    floatingConnection.setImageResource(R.drawable.bluetooth_connect);
//                }
            }
        }
    };
}
